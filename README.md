![thinkpad x220 keyboard][x220kbd]

# tpwkbd - Thinkpad Wireless Keyboard

Converting a Thinkpad X220/T420 keyboard into a Wireless/Wired USB keyboard.

The idea is to have a lightwight small wireless keyboard for my lab so I don't have to deal with a fullsize wired one and a mouse in between my soldering iron and other stuff.
I still need this keyboard to be wired though, to be able to access the BIOS/UEFI of the PC it is connected to. So the plan is to support Bluetooth and USB at the same time or with a simple switch.
I also want to write the least amount of code so I will use the awesome QMK firmware and just add a custom keyboard to it.

There are lot's of similar projects out there but I couldn't find one that completely suits my needs, so I created my own version.

Here are some honorable mentions (I totally stole some ideas and code from them):

- https://www.youtube.com/watch?v=FwnfiDEq4XI
- https://www.youtube.com/watch?v=eVBTEWGJvJc
- https://www.youtube.com/watch?v=8nqvHDSDzKk
- https://github.com/thedalles77/USB_Laptop_Keyboard_Controller/
- https://github.com/lowJ/tp-keyboard
- https://github.com/eyemyth/qmk_firmware/tree/master/keyboards/lenovo_t420
- https://github.com/feklee/thinkpad-qmk/tree/thinkpad-qmk/keyboards/thinkpad

_Project not yet done_

## PCB

The pcb is done with KiCAD and can be found in the [pcb/](https://gitlab.com/XenGi/tpwkbd/-/tree/master/pcb) subdirectory.

As a side project I designed a breakout board for the Thinkpad keyboard connector. It can be found in the [AA01B-S040VA1-breakout](https://gitlab.com/XenGi/tpwkbd/-/tree/master/AA01B-S040VA1-breakout) subdirectory.

## Case

The case is done with FreeCAD and can be found in the [cad/](https://gitlab.com/XenGi/tpwkbd/-/tree/master/cad) subdirectory.

## Firmware

QMK is used as the firmware and can be found [here][firmware].

Compile and flash it with:

```sh
make tpwkbd:default:teensy
```

## Assembly

See [bom][bom] for parts list.

1. Assemble PCB, solder components according to [bom][bom]
2. Flash Firmware from [here][firmware] to Teensy 2.0++
3. Attach pin headers to Teensy 2.0++
4. Plug Teensy 2.0++ into PCB
5. Connect USB-C breakout to Teensy 2.0++
   1. Cut USB-A end of USB-Mini cable
   2. Plug USB-Mini cable into Teensy
   3. Solder other end to USB-C breakout (connect VCC, GND, D+, D-)
6. Connect Battery packs to PCB
7. (Optional) Attach Adafruit Bluetooth module to PCB
8. Screw everything into the case
9. Connect Thinkpad keyboard
10. Put Thinkpad keyboard into the case
11. Connect via USB or Bluetooth and type away!


---

Made with ❤️ and [![qmk logo][qmk_logo]][qmk]. Licensed under [![Creative Commons License][cc-by-nc-img]][cc-by-nc].


[x220kbd]: https://gitlab.com/xengi/tpwkbd/-/raw/master/layout/thinkpad-x220.png
[firmware]: https://gitlab.com/XenGi/tpwkbd-firmware
[bom]: https://gitlab.com/XenGi/tpwkbd/-/blob/master/BOM.md
[qmk_logo]: https://raw.githubusercontent.com/qmk/qmk_firmware/master/docs/gitbook/images/favicon.png
[qmk]: https://qmk.fm/
[cc-by-nc]: http://creativecommons.org/licenses/by-nc/4.0
[cc-by-nc-img]: https://i.creativecommons.org/l/by-nc/4.0/80x15.png
